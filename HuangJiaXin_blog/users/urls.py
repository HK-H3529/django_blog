from django.urls import path
from users.views import Register,ImageCodeView,SmsCodeView,RegisterView

urlpatterns = [
    path('register/', Register().register,name='register'),
    path('imageCode/',ImageCodeView.as_view(),name='imageCode'),
    path('smsCode/',SmsCodeView.as_view(),name='smsCode'),
    path('register/isAcc/',RegisterView.as_view()),
]
